import React, {Component} from 'react';
import {Form, Col, Row, Input, TreeSelect, Select, Button  } from 'antd'
import 'antd/dist/antd.css';
import axios from 'axios';
import './App.css'
import Template from './Template'
import Ticket from './Ticket'
async function apiService(endPoint,method, data, headers){
  const result = await axios({
    url: 'http://localhost:8888/' + endPoint,
    method,
    data,
    headers
  })
  if(result.data)
    return result.data
  else {window.alert('network error')}
}

const {Option } = Select
const layout = {
  labelCol: {
    span: 4,
  },
  wrapperCol: {
    span: 8,
  },
};
const dateInputLayout = {
  labelCol: {
    span: 14,
  },
  wrapperCol: {
    span: 10,
  },
};

const treeInputLayout = {
  labelCol: {
    span: 7,
  },
  wrapperCol: {
    span: 16,
  },
};
const bottomInputLayout = {
  labelCol: {
    span: 11,
  },
  wrapperCol: {
    span: 13,
  },
};
class App extends Component {
  constructor(props) {
    super(props)
    this.formRef = React.createRef()
  }
  state = {
    stationList: [],
    scheduleList: [],
    typeList: [],
    showPrintTicket: false,
    showPrintBill: false,
    data: {
      number: '', 
      ticket_code:'',
      start_code:'', 
      des_code:'',
      time: '',
       train:'' ,
      start_day:'' ,
      start_month:'',
      start_year:'',
      ticket_day:'',
      ticket_month:'',
      ticket_year:'',
      wagon:'',
      chair:'',
      type_code:'',
      name:'',
      price_number:'',
      paper:'',
      price_char:'',
      mst:'',
      origin_price:'',
      name_organization:'',
      tax:'',
      address:'',
      phone:'',
      }
  }
  componentDidMount(){
    this.fetchStation()
    this.fetchSchedule()
    this.fetchType()
    this.fetchTicketCode()
  }
  async fetchStation(){
    const result = await apiService('station')
    if(result.status)
      this.setState({ stationList: result.data})
  }
  async fetchTicketCode(){
    const result = await apiService('bill/ticket_code')
    if(result.status)
    this.formRef.current.setFieldsValue({
      ticket_code: result.data
     })
  }
  async fetchSchedule(){
    const result = await apiService('schedule')
    if(result.status)
      this.setState({ scheduleList: result.data})
  }
  async fetchType(){
    const result = await apiService('type')
    if(result.status){
      const treeData = result.data.map((item, index) => ({
        title: item.name,
        value: index,
        selectable: false,
        children: item.seatDetail.wagons.map((wagon, indexWagon) => ({
          title: `Toa: ${wagon}`,
          value: `${index}-${indexWagon}`,
          selectable: false,
          children: item.seatDetail.chairs.map((chair, indexChair) =>({
            title: `Ghế: ${chair}`,
            value: `${index}-${indexWagon}-${indexChair}`,
          }))
        }))
      }))
      this.setState({ typeList: result.data ,treeData})
    }
  }
  handleChangeTree = (value) => {
    const [indexType, indexWagon, indexChair] = value.split('-').map(item => parseInt(item))
    const type = this.state.typeList[indexType].code
    const wagon = this.state.typeList[indexType].seatDetail.wagons[indexWagon]
    const chair = this.state.typeList[indexType].seatDetail.chairs[indexChair]
    this.setState({ 
      treeValue: `Loại chỗ: ${type} | Toa: ${wagon} | Ghế: ${chair}`
     })
    this.formRef.current.setFieldsValue({
      wagon,
      chair,
      type: this.state.typeList[indexType].name,
      type_code: type
    })
  }
  handleChangeSelect = (e, node, {dataSet, field, value}) => {
    this.formRef.current.setFieldsValue({
      [node.name]: e,
      [field]: dataSet[node.index][value]
    })
  }
  changeScheduleList = (value, node) => {
    this.formRef.current.setFieldsValue({
      time: node.time,
      train: node.train
    })
  }
  handleChange = e => {
    this.formRef.current.setFieldsValue({
      ticket_code: this.formRef.current.getFieldsValue().ticket_code,
     [e.target.name]: e.target.value,
    })
  }
  handleShow =async (type, state, isSubMit) => {
    if(isSubMit){
      const check = await apiService('bill', 'post', this.formRef.current.getFieldsValue())
      console.log(check)
    }
    this.setState({
      [`showPrint${type}`]: state
    })
   
  }
 
render(){
  return (
    <React.Fragment>
      <Form onChange={this.handleChange} ref={this.formRef} style={{marginBottom: 0}}  {...layout}>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="MÃ VÉ"
        name="ticket_code"
      >
      <Input readonly label="asd" name="ticket_code" value={this.state.ticket_code}  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="GA ĐI"
        name="start"
        
      >
      <Input name="start" value={this.state.start}  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        name="start"
        
      >
     <Select onChange={(value, option) =>this.handleChangeSelect(value, option, {dataSet: this.state.stationList, field: 'start_code', value: 'code'})} value={this.state.start}>
       {this.state.stationList.map((item, index) => <Option index={index}  name="start" key={item.code} value={item.name}>{item.name}</Option>)}
     </Select>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="MÃ GA ĐI"
        name="start_code"
        
      >
      <Input type="text" name="start_code" onChange={this.handleChange}  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="GA ĐẾN"
        name="des"
        
        
      >
      <Input  name="des"  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        name="des"
        
      >
      <Select  onChange={(value, option) =>this.handleChangeSelect(value, option, {dataSet: this.state.stationList, field: 'des_code', value: 'code'})}>
       {this.state.stationList.map((item, index) => <Option
        index={index} key={item.code} value={item.name}
        name="des"
        >{item.name}</Option>)}
     </Select>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="MÃ GA ĐẾN"
        name="des_code"
        
      >
      <Input name="des_code"  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="GIỜ ĐI"
        name="time"
        
      >
      <Input  name="time"  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        
      >
     <Select onChange={this.changeScheduleList}>
       {this.state.scheduleList.map(item => <Option 
       key={item._id} 
       value={item._id}
       train={item.train}
       time={item.time}
       >{item.name} | {item.train} | {item.time}</Option>)}
     </Select>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="TÀU"
        name="train"
        
      >
      <Input name="train"  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
   
      <Row >
      <Col span={3} offset={1}>
        <Form.Item
         {...dateInputLayout}
        label="NGÀY ĐI"
        name="start_day"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      <Col span={3}>
        <Form.Item
       {...dateInputLayout}
        label="THÁNG ĐI"
        name="start_month"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      <Col span={3}>
      <Form.Item
        {...dateInputLayout}
        label="NĂM ĐI"
        name="start_year"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      </Row>
      <Row >
      <Col span={3} offset={1}>
        <Form.Item
         {...dateInputLayout}
        label="NGÀY IN VÉ"
        name="ticket_day"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      <Col span={3}>
        <Form.Item
       {...dateInputLayout}
        label="THÁNG IN VÉ"
        name="ticket_month"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      <Col span={3}>
      <Form.Item
        {...dateInputLayout}
        label="NĂM IN VÉ"
        name="ticket_year"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
      </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="TOA"
        name="wagon"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={4}>
        <TreeSelect
        style={{ width: '100%',marginLeft: 10 }}
        value={this.state.treeValue}
        dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
        treeData={this.state.treeData}
        placeholder="Please select"
        onChange={this.handleChangeTree}
      />
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="GHẾ"
        name="chair"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="LOẠI CHỖ"
        name="type"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="KÍ HIỆU LOẠI CHỖ"
        name="type_code"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="HỌ TÊN"
        name="name"
        
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="GIÁ TIỀN BẰNG SỐ"
        name="price_number"
      >
      <Input name="price_number"  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="GIẤY TỜ"
        name="paper"
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="SỐ TIỀN BẰNG CHỮ"
        name="price_char"
      >
      <Input  name="price_char"  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="MÃ SỐ THUẾ"
        name="mst"
      >
      <Input  name="mst"  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="GIÁ CHƯA THUẾ"
        name="origin_price"
      >
      <Input  name="origin_price"  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="TÊN ĐƠN VỊ"
        name="name_organization"
      >
      <Input  name="name_organization" autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="THUẾ GIÁ TRỊ GIA TĂNG"
        name="tax"
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="ĐỊA CHỈ"
        name="address"
      >
      <Input  name="address"  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="SỐ"
        name="number"
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={7}>
        <Form.Item
        {...treeInputLayout}
        label="SỐ ĐT"
        name="phone"
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
        <Col span={6}>
        <Form.Item
        {...bottomInputLayout}
        label="ID"
        name="id"
      >
      <Input  autoComplete="off"/>
      </Form.Item>
        </Col>
      </Row>
    <Template handleShow={this.handleShow} show={this.state.showPrintBill}    data={this.formRef.current ?this.formRef.current.getFieldsValue() : this.state.data}/>}
     <Ticket handleShow={this.handleShow} show={this.state.showPrintTicket}data={this.formRef.current ?this.formRef.current.getFieldsValue() : this.state.data}/>}
     <Button  style={{width:100, height:100, position: 'absolute', top: "40%", left: "50%"}} type="primary" htmlType="submit" onClick={() => this.handleShow('Bill', true,true)}>
       In hóa đơn
     </Button>
     <Button  style={{width:100, height:100, position: 'absolute', top: "40%", left: "60%"}} type="primary" htmlType="submit" onClick={() => this.handleShow('Ticket', true, true)}>
       In vé
     </Button>
      </Form>
    </React.Fragment>
  );
}
}

export default App;
