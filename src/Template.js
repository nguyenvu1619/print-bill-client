import React, { useRef } from 'react';
import logo from './logo.png'
import {Button} from 'antd'
import "./App.css"
import PrintComponents from "react-print-components";
import QRcode from 'qrcode.react'
class  Template extends React.Component {
   
    componentDidUpdate(){
        if(this.props.show){
            this.ref.click()
            this.props.handleShow('Bill', false)
        }
    }
    render(){
    const {number,
        id,
        ticket_code,
        start_code, 
        des_code,
         train, 
        start_day, 
        start_month,
        start_year,
        type_code,
        name,
        price_number,
        price_char,
        mst,
        origin_price,
        name_organization,
        tax,
        address,
        phone,
    } = this.props.data
    const qrCodeValue =  `${ticket_code}_${name&&name.toLowerCase().replace(' ')}`
    return(<React.Fragment>
        <PrintComponents
  trigger={ <div ref={(ref) => this.ref = ref} type="primary" htmlType="submit">
</div>}>
        <div id="bill">
        <div style={{
            width: '100%',
            margin: 'auto',
            textAlign: 'center',
            borderBottom: "1px solid black",
            fontSize: 12
        }}>
            Đơn vị cung cấp giải pháp hóa đơn điện tử:Tổng công ty dịch vụ viễn thông - VNPT Vinaphone, MST:0106869739, Điện thoại:04.37930597
        </div>
        <div style={{ display: 'flex', width: "100%", margin: 'auto', justifyContent: 'space-between',marginTop: '10px'}}>
            <div style={{width: 200}}>
                <img style={{width: '100%'}} src={logo}/>
            </div>
            <div>
                <div style={{fontWeight: 'bold'}}>HÓA ĐƠN GIÁ TRỊ GIA TĂNG</div>
                <div style={{marginTop: 10}}>Ngày 26 tháng 10 năm 2019</div>
            </div>
            <div>
                <div>Mẫu số: 01GTKT0/015</div>
                <div>Kí hiệu: BA/15E</div>
                <div>Số: {number}</div>
                <div>ID: {id}</div>
            </div>
        </div>
        <div style={{marginLeft: 0, marginTop: 50}}>
        <div style={{width: 150, position: 'absolute', right: 0}}>
        <QRcode
    id={qrCodeValue}
    value={{qrCodeValue}}
    size={150}
    level={"H"}
    includeMargin={true}
  />
            </div>
        <div>
        <span style={{width: 40}}> Đơn vị bán hàng:</span> <span style={{fontWeight: 600}}>CÔNG TY TNHH MỘT THÀNH VIÊN VẬN TẢI ĐƯỜNG SẮT SÀI GÒN</span>
        </div>
        <div>
        <span>  Địa chỉ:</span><span style={{fontWeight: 600}}>Số 136 Hàm Nghi, Phường Bến Thành, Quận 1, Thành phố Hồ Chí Minh, Việt Nam</span>
        </div>
        <div style={{display: 'flex',width:'80%', justifyContent: 'space-between'}}>
            <div style={{width: "60%"}}>
            <span>MST:</span> <span style={{marginLeft: 40,fontWeight: 600}}>030112037</span>
            </div>
            <div style={{width: "40%"}}>
            Điện thoại: <span style={{fontWeight: 600}}>(84-8) 38290199</span>
            </div>
        </div>
        <div style={{display: 'flex',width:'80%', justifyContent: 'space-between'}}>
            <div style={{width: "60%"}}>
            <span>Tài khoản:</span> <span style={{fontWeight: 600}}>102010000115386</span>
            </div>
            <div style={{width: "20%"}}>
            Mã NV: <span style={{fontWeight: 600}}>sgontd</span>
            </div>
            <div style={{width: "20%"}}>
            Mã CV: <span style={{fontWeight: 600}}>SGO32</span>
            </div>
        </div>
        <div>
          <span> Tại ngân hàng:</span> <span style={{fontWeight: 600}}>CÔNG TY TNHH MỘT THÀNH VIÊN VẬN TẢI ĐƯỜNG SẮT SÀI GÒN</span>
        </div>
        <div>
    <span>Họ tên khách hàng:</span> <span style={{fontWeight: 600}}>{name}</span>
        </div>
        <div>
    <span>Tên đơn vị:</span><span style={{fontWeight: 600}}>{name_organization}</span>
        </div>
        <div>
    <span>Địa chỉ:</span> <span style={{fontWeight: 600}}>{address}</span>
        </div>
        <div style={{display: 'flex',width:'80%', justifyContent: 'space-between'}}>
            <div style={{width: "60%"}}>
            <span>MST:</span> <span style={{marginLeft: 40,fontWeight: 600}}>{mst}</span>
            </div>
            <div style={{width: "40%"}}>
            Điện thoại: <span style={{fontWeight: 600}}>{phone}</span>
            </div>
        </div>
        <div style={{display: 'flex',width:'80%', justifyContent: 'space-between'}}>
            <div style={{width: "60%"}}>
            <span>Hình thức thanh toán:</span> <span style={{marginLeft: 40}}>Tiền mặt</span>
            </div>
            <div style={{width: "40%"}}>
            Tài khoản:
            </div>
        </div>
        </div>
        <table>
  <tr>
    <th>STT</th>
    <th>Mã vé</th>
    <th>Nội dung</th>
    <th>DVT</th>
    <th>Số lượng</th>
    <th>Giá chưa thuế</th>
    <th>Tiền thuế GTGT (thuế suất 10%)</th>
    <th>Phí bảo hiểm HK</th>
    <th>Thành tiền</th>
  </tr>
  <tr>
    <td>1</td>
    <td>{ticket_code}</td>
    <td>Vé HK: {start_code} {des_code} - {type_code} / {train} -{start_year}-{start_month}-{start_day}</td>
    <td>Vé</td>
    <td>1</td>
    <td>{origin_price}</td>
    <td>{tax}</td>
    <td>1.000</td>
    <td>{price_number}</td>
  </tr>
  <tr>
    <td colSpan="3">Tổng cộng</td>
    <td>X</td>
    <td>1</td>
    <td>{origin_price}</td>
    <td>{tax}</td>
    <td>1.000</td>
    <td>{price_number}</td>
  </tr>
  <tr>
    <td colSpan="9">Số tiền viết bằng chữ: {price_char}</td>
  </tr>
</table>
</div>
</PrintComponents>
        </React.Fragment>
    )
}
}

export default Template