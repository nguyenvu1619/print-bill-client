import React, { useRef } from 'react';
import {Button} from 'antd'
import "./App.css"
import PrintComponents from "react-print-components";
import QRcode from 'qrcode.react'
class  Template extends React.Component {
    componentDidUpdate(){
        if(this.props.show){
            this.ref.click()
            this.props.handleShow('Ticket', false)
        }
    }
    render(){
    const {
        ticket_code,
        start, 
        des,
        time,
         train, 
        start_day, 
        start_month,
        start_year,
        ticket_day,
        ticket_month,
        ticket_year,
        wagon,
        chair,
        type_code,
        name,
        price_number,
        paper,
    } = this.props.data
    const qrCodeValue =  `${ticket_code}_${name&&name.toLowerCase().replace(' ')}`
    return(<React.Fragment>
        <PrintComponents
  trigger={ <div ref={(ref) => this.ref = ref}>
</div>}>
     <div>
         <div className="header-ticket">
         <div style={{
             fontSize: 15,
             fontWeight:"bold",
         }}>TỔNG CÔNG TY ĐƯỜNG SẮT VIỆT NAM</div>
         <div style={{
             fontSize: 13,
             fontWeight:600,
         }}>CÔNG TY TNHH MỘT THÀNH VIÊN</div>
         <div style={{
             fontSize: 13,
             fontWeight:600,
         }}>VẬN TẢI ĐƯỜNG SẮT SÀI GÒN</div>
         <div style={{
             fontSize: 13,
             fontWeight:600,
         }}>THẺ LÊN TÀU HỎA</div>
         <div style={{
             fontSize: 22,
             fontWeight:"bold",
         }}>BOARDING BOAR</div>
            <div>
            <QRcode
    id={qrCodeValue}
    value={{qrCodeValue}}
    size={150}
    level={"H"}
    includeMargin={true}
  />
  </div>
  <div>Mã vé/TicketID : {ticket_code}</div>
  </div>
  <div className="field-ticket-container" style={{width: "40%", justifyContent: 'space-around'}}>
      <div>
          <div className="field-ticke">Ga đi</div>
          <div>{start}</div>
      </div>
      <div>
          <div className="field-ticke">Ga đến</div>
          <div>{des}</div>
      </div>
  </div>
            
     </div>
     <div >
     <div className="field-ticket-container">
         <div className="field-ticket">Tàu/Train: </div>
         <div>{train}</div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Ngày đi/Date: </div>
        <div>{`${start_day}/${start_month}/${start_year}`}</div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Giờ đi: </div>
         <div>{time}</div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Toa/Coach: </div>
         <div>{wagon} </div>
         <div className="field-ticket" style={{marginLeft: 4}}>Chỗ/Seat: </div>
         <div>{chair} </div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Loại vé/Ticket: </div>
         <div>Toàn vé </div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Loại chỗ/Class: </div>
         <div>{type_code} </div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Họ tên/Name: </div>
         <div>{name} </div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Giấy tờ/Passport: </div>
         <div>{paper}</div>
     </div>
     <div className="field-ticket-container">
         <div className="field-ticket">Giá/Price: </div>
         <div>{price_number}</div>
     </div>
     <div className="footer-ticket">
         <div>
         Ghi chú: Thẻ lên tàu không phải là hóa đơn GTGT và không có giá trị thanh toán. Để tra cứu hóa đơn điện tử xin vui lòng truy cập địa chỉ trang web:
         </div>
         <div>
         This boarding pass is not an official invoice. To receive electronic invoice please visit wesite address at:
         <div>http://doadon.vantaiduongsathanoi.vn</div>
         </div>
         <div>
         Để đảm bảo quyền lợi quý khách vui lòng mang theo thẻ tàu hỏa cùng với giấy tờ tùy thân trong suốt hành trình
         </div>
         <div>
         Ngày in: {`${ticket_day}/${ticket_month}/${ticket_year}`}
         </div>
         <div>
         Ngày thanh toán: {`${ticket_day}/${ticket_month}/${ticket_year}`}
         </div>
         <div>
         Serial: WEB
         </div>
     </div>
     </div>
</PrintComponents>
        </React.Fragment>
    )
}
}

export default Template